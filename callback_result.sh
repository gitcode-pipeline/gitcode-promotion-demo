echo $1
result="很遗憾，流水线运行失败，再重新运行一次试试吧~"
if [ "$1" = "COMPLETED" ];then
      result="恭喜您，流水线运行成功了~"
elif [ "$1" = "CANCELED" ];then
      result="您取消了流水线运行，后续有需要可以重试或者再次运行~"
else
      result="很遗憾，流水线运行失败，再重新运行一次试试吧~"
fi
rm -rf README.md
echo "[![Build Status](https://gitcode.com/gitcode-pipeline/gitcode-promotion-demo/workflows/test.yml/badge.svg)](https://gitcode.com/gitcode-pipeline/gitcode-promotion-demo/pipeline?file=test.yml) " > README.md
cat ./templates/readme-template-pre.md >> README.md
echo -n $result >> README.md
cat ./templates/readme-template-post.md >> README.md